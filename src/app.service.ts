import { Injectable } from '@nestjs/common';
import { sendMail } from './utils/sendMail';

@Injectable()
export class AppService {
  getHello(): any {
    return {name:'nodemailer'};
  }
  async sendMail(email:string,message:string){
    await sendMail(email,message);
  } 
}
